<?php
    
    class GTA extends Bot  // Change This
    {
        public $name = 'gta';  // Change This (cache file name)
        public $channel_id = 324648723542179840; // Change This
        public $server_id = 245353189552029697; // Change This
        public $role_id = 330142497256767498; // Change This
        
        // Alter this as needed
        public function check()
        {
            $html = @file_get_contents('https://socialclub.rockstargames.com/news/tag/722/1/announcements');
            if($html) {
                $doc = new DOMDocument();
                $doc->loadHTML($html);
                $xml = simplexml_import_dom($doc);
                $items = $xml->xpath('//div[contains(@class, "story list")]');
                $url = (string)$items[0]->div[1]->a['href'];
                if(!$this->__cache($url)) {
                    if(stristr($url, 'GTA-Online')) { // Ignore news not related to GTA Online
                        $this->__sendMessage(sprintf('https://rockstargames.com%s', $url));
                    }
                }
            }
        }
    }