<?php
    
    class TheForest extends Bot  // Change This
    {
        public $name = 'theforest';  // Change This (cache file name)
        public $channel_id = 325530924517294081; // Change This
        public $server_id = 245353189552029697; // Change This
        public $role_id = 330143875429564436; // Change This
        
        // Alter this as needed
        public function check()
        {
            $html = @file_get_contents('http://survivetheforest.com/');
            if($html) {
                $doc = new DOMDocument();
                $doc->loadHTML($html);
                $xml = simplexml_import_dom($doc);
                $items = $xml->xpath('//h2[contains(@class, "entry-title")]');
                $url = (string)$items[0]->a['href'];
                if(!$this->__cache($url)) {
                    $this->__sendMessage($url);
                }
            }
        }
    }