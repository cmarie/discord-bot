<?php
    
    class Empyrion extends Bot  // Change This
    {
        public $name = 'empyrion';  // Change This (cache file name)
        public $channel_id = 325530944867926016; // Change This
        public $server_id = 245353189552029697; // Change This
        public $role_id = 330143153141317643; // Change This
        
        // Alter this as needed
        public function check()
        {
            $html = @file_get_contents('http://empyriononline.com/');
            if($html) {
                $doc = new DOMDocument();
                $doc->loadHTML($html);
                $xml = simplexml_import_dom($doc);
                $items = $xml->xpath('//div[contains(@class, "recentNews")]');
                $url = (string)$items[0]->div[0]->div->div->a['href'];
                if(!$this->__cache($url)) {
                    $this->__sendMessage(sprintf('http://empyriononline.com/%s', $url));
                }
            }
        }
    }