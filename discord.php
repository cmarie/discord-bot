<?php
    libxml_use_internal_errors(true);
    include __DIR__ . '/vendor/autoload.php';
    use Discord\Discord;
    use Dotenv\Dotenv;
    
    $dotenv = new Dotenv(__DIR__);
    $dotenv->load();
    define('WWW_ROOT', env('WWW_ROOT'));
    require WWW_ROOT . '/bot.php';
    
    class DiscordBot
    {
        public function __construct($update_interval)
        {
            $discord = new Discord([
                'token' => env('TOKEN')
            ]);
            $discord->on('ready', function (Discord $discord) use ($update_interval) {
                $discord->loop->addPeriodicTimer($update_interval, function () use ($discord) {
                    foreach(glob(WWW_ROOT . '/bots/*.php') as $file) {
                        include_once $file;
                        $class = str_replace(WWW_ROOT . '/bots/', null, str_replace('.php', null, $file));
                        $game = new $class($discord);
                        $game->check();
                    }
                });
            });
            $discord->run();
        }
    }
    
    $bot = new DiscordBot(env('INTERVAL'));
    