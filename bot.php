<?php
    
    class Bot
    {
        public $name; // Don't Touch
        public $discord; // Don't Touch
        public $server_id; // Change This
        public $channel_id; // Don't Touch
        public $role_id; // Don't Touch
        
        public function __construct($discord)
        {
            $this->discord = $discord;
        }
        
        public function __cache($url)
        {
            $dir = WWW_ROOT . '/cache';
            if(!is_dir($dir)) {
                mkdir($dir, 0777);
            }
            $check = @file_get_contents($dir . '/' . $this->name);
            if($url != null && $check != $url) {
                file_put_contents($dir . '/' . $this->name, $url);
                
                return false;
            }
            
            return true;
        }
        
        public function __sendMessage($text)
        {
            /**
             * Helper Data
             *
             * @var $guild   \Discord\Parts\Guild\Guild
             * @var $channel \Discord\Parts\Channel\Channel
             */
            // Gets server information
            $guild = $this->discord->guilds->get('id', $this->server_id);
            // Gets specific channel on server
            $channel = $guild->channels->get('id', $this->channel_id);
            // Sends message to the specific channel
            if($this->role_id) {
                $channel->sendMessage(sprintf('<@&%s>, %s', $this->role_id, $text));
            } else {
                $channel->sendMessage($text);
            }
        }
    }