# Installation
`cd /path/to/install/directory`

`curl -s http://getcomposer.org/installer | php`

`php composer.phar install`

Rename `.env.example` to `.env`

Modify .env to suit your application